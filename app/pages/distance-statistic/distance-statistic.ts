import {Component} from '@angular/core';
import {Modal, NavController} from 'ionic-angular';
import { DistanceStatisticService } from './distance-statistic.service';
import { distanceStatisticItem } from './distance-statistic-item';

@Component({
    templateUrl: 'build/pages/distance-statistic/distance-statistic.html',
    providers: [DistanceStatisticService]
})
export class DistanceStatistic {
    DistanceStatisticList:any;
    items:Array<{distance: number, date: string}>;

    constructor(private DistanceStatisticService:DistanceStatisticService, public nav:NavController) {
        this.nav = nav;
        this.getDistanceStatistic();
    }

    presentProfileModal() {
        let profileModal = Modal.create(distanceStatisticItem);
        profileModal.onDismiss(data => {
            if (data) {
                this.DistanceStatisticService.addDistanceStatisticItem(data)
                    .then((result)=> {
                        this.getDistanceStatistic();
                    });
            }
        });
        this.nav.present(profileModal);
    }

    getDistanceStatistic() {
        this.DistanceStatisticService.getDistanceStatisticList()
            .then((result)=> {
                this.items = [];
                if (result.res !== undefined && result.res.rows !== undefined) {
                    for (let i = 0; i < result.res.rows.length; i++) {
                        this.items.push(result.res.rows.item(i));
                    }
                }
            });
    }

    deleteDistanceStatisticItem(item) {
        this.DistanceStatisticService.deleteDistanceStatisticItem(item)
            .then(()=> {
                this.getDistanceStatistic();
            });
    }

}

