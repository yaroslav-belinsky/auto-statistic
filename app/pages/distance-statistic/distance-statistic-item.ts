import {Component} from '@angular/core';
import {ViewController} from 'ionic-angular';
import * as moment from 'moment';

@Component({
    templateUrl: 'build/pages/distance-statistic/distance-statistic-new-item-modal.html'
})
export class distanceStatisticItem {
    item:{distance: number, date: string};
    constructor(public viewCtrl:ViewController) {
        this.item = {
            distance: 0,
            date: moment().format('YYYY-MM-DD').toString()
        };
    }

    submit() {
        this.viewCtrl.dismiss(this.item);
    }

    close() {
        this.viewCtrl.dismiss(false);
    }

}