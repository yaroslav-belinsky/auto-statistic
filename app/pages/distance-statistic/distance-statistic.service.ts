import { Injectable } from '@angular/core';
import {Platform, Storage, SqlStorage} from 'ionic-angular';

@Injectable()
export class DistanceStatisticService {
    storage:any = new Storage(SqlStorage);

    constructor(platform:Platform) {
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.storage.query('CREATE TABLE IF NOT EXISTS distance_statistic (id INTEGER PRIMARY KEY AUTOINCREMENT, distance NUMBER, date TEXT)').then((data) => {
                console.log("TABLE CREATED -> " + JSON.stringify(data));
            }, (error) => {
                console.log("ERROR -> " + JSON.stringify(error.err));
            });
            //
            //this.storage.query('DROP TABLE  distance_statistic').then((data) => {
            //    console.log("TABLE DROP -> " + JSON.stringify(data));
            //}, (error) => {
            //    console.log("ERROR -> " + JSON.stringify(error.err));
            //});
        });
    }

    getDistanceStatisticList() {
        return this.storage.query("SELECT * FROM distance_statistic");
    }

    addDistanceStatisticItem(item) {
       return this.storage.query("INSERT INTO distance_statistic (distance, date) VALUES ('" + item.distance + "', '" + item.date + "')");
    }

    deleteDistanceStatisticItem(item) {
        return this.storage.query("DELETE FROM distance_statistic WHERE id = " + item.id);
    }


}