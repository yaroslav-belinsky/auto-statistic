import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import { CHART_DIRECTIVES, Highcharts } from 'angular2-highcharts';
import { DistanceStatisticService } from '../distance-statistic/distance-statistic.service';
import * as moment from 'moment';

Highcharts.setOptions({
    colors: ['#058DC7', '#50B432', '#ED561B']
});

@Component({
    templateUrl: 'build/pages/home/home.html',
    directives: [CHART_DIRECTIVES],
    providers: [DistanceStatisticService]
})
export class HomePage {
    distance:Array<number>;
    dates:Array<any>;
    constructor(private navController:NavController, public DistanceStatisticService: DistanceStatisticService) {
        this.getDistanceStatistic();
    }

    getDistanceStatistic() {
        this.DistanceStatisticService.getDistanceStatisticList()
            .then((result)=> {
                this.distance = [];
                this.dates = [];
                if (result.res !== undefined && result.res.rows !== undefined) {
                    var prevDistance:number = 0;
                    for (let i = 0; i < result.res.rows.length; i++) {
                        if(prevDistance < result.res.rows.item(i).distance) {
                            this.distance.push((result.res.rows.item(i).distance - prevDistance));
                            prevDistance = result.res.rows.item(i).distance;
                        } else {
                            prevDistance = result.res.rows.item(i).distance;
                        }
                        this.dates.push(moment(result.res.rows.item(i).date).format("DD-MM-YY"));

                    }
                    this.options = {
                        title : { text : 'Distance' },
                        series: [{
                            data: this.distance
                        }],
                        xAxis: {
                            categories: this.dates
                        },
                    };
                }
            });
    }
    options: Object;
}
