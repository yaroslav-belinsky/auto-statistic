import { Injectable } from '@angular/core';
import {Platform, Storage, SqlStorage} from 'ionic-angular';

@Injectable()
export class GasolineRechargeService {
    storage:any = new Storage(SqlStorage);

    constructor(platform:Platform) {
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.storage.query('CREATE TABLE IF NOT EXISTS gasoline_recharge (id INTEGER PRIMARY KEY AUTOINCREMENT, count NUMBER, amount NUMBER, date TEXT)').then((data) => {
                console.log("TABLE CREATED -> " + JSON.stringify(data));
            }, (error) => {
                console.log("ERROR -> " + JSON.stringify(error.err));
            });

            //this.storage.query('DROP TABLE  gasoline_recharge').then((data) => {
            //    console.log("TABLE DROP -> " + JSON.stringify(data));
            //}, (error) => {
            //    console.log("ERROR -> " + JSON.stringify(error.err));
            //});
        });
    }

    getGasolineRechargeList() {
        return this.storage.query("SELECT * FROM gasoline_recharge");
    }

    addGasolineRechargeItem(item) {
       return this.storage.query("INSERT INTO gasoline_recharge (count, amount, date) VALUES (" + item.count + ", " + item.amount + ", '" + item.date + "')");
    }

    deleteGasolineRechargeItem(item) {
        return this.storage.query("DELETE FROM gasoline_recharge WHERE id = " + item.id);
    }


}