import {Component} from '@angular/core';
import {ViewController} from 'ionic-angular';
import * as moment from 'moment';

@Component({
    templateUrl: 'build/pages/gasoline-recharge/gasoline-recharge-new-item-modal.html'
})
export class gasolineRechargeItem {
    item:{count: number, amount: number, date: string};
    constructor(public viewCtrl:ViewController) {
        this.item = {
            count: 0,
            amount: 0,
            date: moment().format('YYYY-MM-DD').toString()
        };
    }

    submit() {
        this.viewCtrl.dismiss(this.item);
    }

    close() {
        this.viewCtrl.dismiss(false);
    }

}