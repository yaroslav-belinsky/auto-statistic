import {Component} from '@angular/core';
import {Modal, NavController} from 'ionic-angular';
import { GasolineRechargeService } from './gasoline-recharge.service';
import { gasolineRechargeItem } from './gasoline-recharge-item';

@Component({
    templateUrl: 'build/pages/gasoline-recharge/gasoline-recharge.html',
    providers: [GasolineRechargeService]
})
export class GasolineRecharge {
    gasolineRechargeList:any;
    items:Array<{count: number, amount: string, date: string}>;

    constructor(private GasolineRechargeService:GasolineRechargeService, public nav:NavController) {
        this.nav = nav;
        this.getGasolineRecharge();
    }

    presentProfileModal() {
        let profileModal = Modal.create(gasolineRechargeItem);
        profileModal.onDismiss(data => {
            if (data) {
                this.GasolineRechargeService.addGasolineRechargeItem(data)
                    .then((result)=> {
                        this.getGasolineRecharge();
                    });
            }
        });
        this.nav.present(profileModal);
    }

    getGasolineRecharge() {
        this.GasolineRechargeService.getGasolineRechargeList()
            .then((result)=> {
                this.items = [];
                if (result.res !== undefined && result.res.rows !== undefined) {
                    for (let i = 0; i < result.res.rows.length; i++) {
                        this.items.push(result.res.rows.item(i));
                    }
                }
            });
    }

    deleteGasolineRechargeItem(item) {
        this.GasolineRechargeService.deleteGasolineRechargeItem(item)
            .then(()=> {
                this.getGasolineRecharge();
            });
    }

}

